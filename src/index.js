import express from 'express';

let app = express();

app.get('/', (req, res) => {
  res.send('Node Starter 1.0.0');
});

app.listen(8000);
